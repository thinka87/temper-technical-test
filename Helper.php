<?php

/**
 * Return chart data from CSV file
 * @param  file  $file
 * @return array $chart_data
 */
class Helper {

    function getChartData($file) {

        $row = 0; //ignore title row
        $user_data_list = array();

        while (($data = fgetcsv($file, 1000, ",")) !== FALSE) {
            if ($row > 0 && is_array($data)) {
                $format_data_array = $this->formatDataAsArray($data);
                if ($format_data_array) {
                    $user_data_list[] = $format_data_array;
                }
            }
            $row++;
        }

        if (empty($user_data_list)) {
            echo "Invalid chart data found in CSV";
            exit();
        }


        $unqiue_date_list = $this->getUniqueDateList($user_data_list);
        $week_number_list_unique = array_unique($this->getWeekNumberOfEachUniqueDate($unqiue_date_list));

        $chart_data = array();

        foreach ($week_number_list_unique as $week_number) {
            $checked_data = $this->checkData($user_data_list, $week_number);
            $week_number_ex = explode('-', $week_number);
            $monday = $this->getMonday($week_number_ex[1], $week_number_ex[0]);

            $chart_row = array();
            $chart_row[] = $monday;
            $chart_row[] = $week_number;
            $chart_row['step1'] = $checked_data[0];
            $chart_row['step2'] = $checked_data[1];
            $chart_row['step3'] = $checked_data[2];
            $chart_row['step4'] = $checked_data[3];
            $chart_row['step5'] = $checked_data[4];
            $chart_row['step6'] = $checked_data[5];
            $chart_row['step7'] = $checked_data[6];
            $chart_row['step8'] = $checked_data[7];

            $chart_data[] = $chart_row;
        }



//    echo "<pre>";
//    print_r($chart_data);
//    exit();

        return $this->generateChartData($chart_data);
    }

    /**
     * Return chart data for high charts
     * @param array char_data
     * @return array chart_json
     */
    function generateChartData($chart_data) {


        $chart_json ["chart"] = array(
            "type" => "line"
        );
        $chart_json ["title"] = array(
            "text" => "Tmpr Onboarding Flow - Weekly Retention Curves"
        );

        $chart_json ["xAxis"] = array(
            "categories" => array()
        );
        $chart_json ["tooltip"] = array(
            "valueSuffix" => "%"
        );


        $categoryArray = array(
            '0',
            '20',
            '40',
            '50',
            '70',
            '90',
            '99',
            '100'
        );


        $chart_json ["yAxis"] = array(
            "title" => array(
                "text" => "Total Onboarded"
            ),
            'labels' => array(
                'format' => '{value}%'
            ),
            'min' => '0',
            'max' => '100'
        );


        foreach ($chart_data as $cd) {
            $dataArray = array();

            for ($i = 1; $i <= 8; $i++) {

                if ($i == 1) {
                    $dataArray[] = 100; //On the first step (X=0) 100% of the users are still active, so all charts start at X=0, Y=100%
                } else {
                    $dataArray[] = round(($cd['step' . $i] / $cd['step1']) * 100);
                }
            }


            $chart_json ["series"] [] = array(
                "name" => $cd[0],
                "data" => $dataArray
            );
        }

        $chart_json ["xAxis"] = array(
            "categories" => $categoryArray
        );

        return $chart_json;
    }

    /**
     * Return valid and calculated chart data for high charts
     * @param type $user_data_list
     * @param type $week_number
     * @return type array
     */
    function checkData($user_data_list, $week_number) {

        $step1 = 0;
        $step2 = 0;
        $step3 = 0;
        $step4 = 0;
        $step5 = 0;
        $step6 = 0;
        $step7 = 0;
        $step8 = 0;

        $i = 0;

        foreach ($user_data_list as $ud) {


            $date = new DateTime($ud[1]);
            $nameOfDay = date('D', strtotime($ud[1]));
            $week = $date->format("W");
            $year = $date->format("Y");
            if ($nameOfDay == 'Sun') {
                $week = $week + 1;
            }
            $current_week_number = $year . '-' . $week;

            if ($week_number == $current_week_number) {

                if ($ud[2] <= 100) {

                    $step1++;
                }
                if ($ud[2] > 0 && $ud[2] <= 100) {
                    $step2++;
                }
                if ($ud[2] > 20 && $ud[2] <= 100) {
                    $step3++;
                }
                if ($ud[2] > 40 && $ud[2] <= 100) {
                    $step4++;
                }
                if ($ud[2] > 50 && $ud[2] <= 100) {
                    $step5++;
                }
                if ($ud[2] > 70 && $ud[2] <= 100) {
                    $step6++;
                }
                if ($ud[2] > 90 && $ud[2] <= 100) {
                    $step7++;
                }
                if ($ud[2] == 100) {
                    $step8++;
                }
            }
        }


        $result = array();
        $result[] = $step1;
        $result[] = $step2;
        $result[] = $step3;
        $result[] = $step4;
        $result[] = $step5;
        $result[] = $step6;
        $result[] = $step7;
        $result[] = $step8;

        return $result;
    }

    /**
     * Return unique date list
     * @param type $user_data_list
     * @return type array
     */
    function getUniqueDateList($user_data_list) {

        $date_list = array();
        foreach ($user_data_list as $dl) {

            $date_list[] = $dl[1];
        }

        return array_unique($date_list);
    }

    /**
     * Return start date of each week
     * @param type $unqiue_date_list
     * @return type array
     */
    function getWeekStartOfEachUniqueDate($unqiue_date_list) {

        $start_date_list = array();

        foreach ($unqiue_date_list as $date) {
            $start_date_list[] = date('Y-m-d', strtotime('previous monday', strtotime($date)));
        }
        return $start_date_list;
    }

    /**
     * Return start date of each specific week
     * @param type $week
     * @param type $year
     * @return type date
     */
    function getMonday($week, $year) {

        $timestamp = mktime(0, 0, 0, 1, 1, $year) + ( $week * 7 * 24 * 60 * 60 );
        $timestamp_for_monday = $timestamp - 86400 * ( date('N', $timestamp) - 1 );
        $date_for_monday = date('Y-m-d', $timestamp_for_monday);

        return $date_for_monday;
    }

    /**
     * Return week number of each date
     * @param type $unqiue_date_list
     * @return type array
     */
    function getWeekNumberOfEachUniqueDate($unqiue_date_list) {

        $week_number_list = array();
        foreach ($unqiue_date_list as $date) {
            
            $date = new DateTime($date);
            $week = $date->format("W");
            $year = $date->format("Y");
            $new_date = $date->format("Y-m-d");
            //$monday=date('Y-m-d', strtotime('previous monday', strtotime($new_date)));
            $week_number_list[] = $year . '-' . $week;
            
        }
        
        return $week_number_list;
    }

    /**
     * Return only valid date list
     * @param type $arr_row_data
     * @return type array
     */
    function formatDataAsArray($arr_row_data) {

        $data_array = $this->dataExplode(trim($arr_row_data[0]));
        $is_valid_date = $this->validateDate($data_array[1]);
        if ($is_valid_date) {
            return $data_array;
        }

        return false;
    }

    /**
     * Return explode data string as array
     * @param string $data_string
     * @return array
     */
    function dataExplode($data_string) {

        return explode(";", $data_string);
    }

    /**
     * Return valid date
     * @param string $date
     * @return bool
     */
    function validateDate($date) {

        return (bool) strtotime($date);
    }

}
