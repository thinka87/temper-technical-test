# temper-technical-test
# Installation

Please clone repository into PHP Documents folder and 
add below code block into ngnix default conf file 

server {
    listen 80;
    listen [::]:80;
    root {add_document_root_here};
    index  index.php;
    server_name  {add_server_name_here};;

    location / {
        try_files $uri $uri/ =404;       
}
