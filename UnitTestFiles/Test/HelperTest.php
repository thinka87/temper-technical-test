<?php
declare(strict_types = 1);
namespace UnitTestFiles\Test;

include '../../Helper.php';
use PHPUnit\Framework\TestCase;

final class HelperTest extends TestCase {

    public function testDateIsValid() {
        // test to ensure that the object from an fsockopen is valid
        $helper = new \Helper();
        $date = '2018-08-05';
        $this->assertTrue($helper->validateDate($date) !== false);
    }
    
    public function testDateIsInvalid() {
        // test to ensure that the object from an fsockopen is valid
        $helper = new \Helper();
        $date = '201808-05';
        $this->assertFalse($helper->validateDate($date) !== true);
    }
    
    public function testWeekNumber(){
        
        $helper = new \Helper();
        $this->assertEquals(array('2016-29'),$helper->getWeekNumberOfEachUniqueDate(array('2016-07-19')),'');
        
    }

}
